from configparser import ConfigParser
from loguru import logger

class NEAT:

    def __init__(self, configFileLocation):
        self.config = ConfigParser()
        self.config.read(configFileLocation)

        # Setup the logger
        logger.add(self.config['LOGGING']['FileLocation'])

    def run(self):
        i = 0