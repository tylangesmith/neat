from setuptools import setup
import os

os.system('pip install git+https://tylangesmith@bitbucket.org/tylangesmith/neuralnetwork.git@master')

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
   name='neat',
   version='0.1',
   license="MIT",
   long_description=long_description,
   description='',
   author='Ty Lange-Smith',
   author_email='tylangesmith@gmail.com',
   packages=['neat'],
   install_requires=['neuralnetwork', 'networkx', 'loguru']
)