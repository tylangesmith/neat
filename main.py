from neat import NEAT

class Main:

    def run(self):
        neat = NEAT('./config/neat.cfg')

if __name__ == '__main__':
    main = Main()
    main.run()