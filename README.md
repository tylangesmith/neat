# NEAT: Neural Evolution of Augmenting Topologies

Project description

## Usage

Give some description as to how the project can be used

```python
# My awesome code example
codeExample = MyAwesomeCodeExample()
```

## Authors

* Ty Lange-Smith - [Website](https://www.tylangesmith.com.au)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details